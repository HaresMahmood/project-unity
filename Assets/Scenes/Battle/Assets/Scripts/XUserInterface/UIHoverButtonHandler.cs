using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///
/// </summary>
public interface UIHoverButtonHandler
{
    #region Variables



    #endregion

    #region Miscellaneous Methods

    void DeselectComponents(UserInterfaceSubComponent selectedComponent);

    #endregion
}

