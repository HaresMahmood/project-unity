﻿using UnityEngine;
using UnityEngine.Events;

public class DialogEventHandler : MonoBehaviour
{
    public Dialog dialog;
    public UnityEvent eventHandler;
}
