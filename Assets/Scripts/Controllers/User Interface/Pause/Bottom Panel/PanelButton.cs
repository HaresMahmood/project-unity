﻿using UnityEngine;

/// <summary>.
///
/// </summary>
[System.Serializable]
public class PanelButton
{
    #region Variables

    public string text;
    public Sprite icon;
    public bool isAnimated;

    #endregion

    #region Unity Methods

    /// <summary>
    /// Start is called before the first frame update.
    /// </summary>
    private void Start()
    {
        
    }

    /// <summary>
    /// Update is called once per frame.
    /// </summary>
    private void Update()
    {
        
    }

    #endregion
}
